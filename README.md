
```
#!python

from gpiozero import MotionSensor
import RPi.GPIO as GPIO
import time
import datetime
file = open("final.txt","w")

#Pin Definitions
ledPin=17
ledPin2 =27
buttonPin=22
buttonPin2= 23
umbrellaPin = 18
motion = 4
photo = 3
watersen=2

#Pin Setup:
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

GPIO.setup(ledPin,GPIO.OUT)													#Button Section
GPIO.setup(ledPin2, GPIO.OUT)
GPIO.setup(buttonPin,GPIO.IN)
GPIO.setup(buttonPin2, GPIO.IN)
GPIO.setup(watersen, GPIO.IN)	#water sensor
#GPIO.setup(11, GPIO.IN)														#Motion Sensor Section
pir = MotionSensor(4)
#GPIO.setup(motion, GPIO.OUT)
def RCtime(RCpin):
	reading=0;
	GPIO.setup(RCpin, GPIO.OUT)														#light sensor section
	GPIO.output(RCpin,GPIO.LOW)
	time.sleep(0.1)
	#change the photo back in input
	GPIO.setup(RCpin,GPIO.IN)
	#Count untill the pin goes high
	while(GPIO.input(RCpin)==GPIO.LOW):
		reading+=1
	return reading


#motor motor motror
#GPIO.setmode(GPIO.BOARD)
GPIO.setup(21, GPIO.OUT)
p= GPIO.PWM(21, 50)
p.start(2.5)																#Servo Motor Section

#GPIO.setup()																#Water Sensor Section

#Initial State for LED:
GPIO.output(ledPin, GPIO.LOW)
GPIO.output(ledPin, GPIO.LOW)

def openFan():
	p.ChangeDutyCycle(12.5)
	time.sleep(1)
	return True
def closeFan():
	p.ChangeDutyCycle(2.5)
	time.sleep(1)
	return False

print("Simple Python GPIO start! Press CTRL+C to exit")
try:
	while 1:
		isopen = False														#Umbrella starts out closed
		manu_button = False													#On/Off starts in off position
		auto_button = False
		water = False													#Auto starts in off position
		toomuchlight= False
		#Light Sensor setup
		if RCtime(photo)<=10000:
			toomuchlight = True


		#Button and LED Setup
	        if GPIO.input(buttonPin2):											#if auto is turned on
        		auto_button = True
        		GPIO.output(ledPin2, GPIO.HIGH)									#second LED turns on
            		time.sleep(1)
           		# print(GPIO.input(buttonPin2))									#print auto_button input

		#Motion Sensor Setup
		if pir.motion_detected:
			print("Motion detected")
			time.sleep(.1)


#		if isopen and GPIO.input(11):										#if the umbrella is open AND the motion sensor is on
#			GPIO.output(motion, 1)											#motion will return detected
#			time.sleep(1)
#		else:
#			GPIO.output(motion, 0)											#otherwise, motion returns undetected
#			time.sleep(1)


		#Water Sensor Setup
		if not( GPIO.input(watersen)):
			water = True
		#Servo Motor Setup

		#Payoff
	        while auto_button:													#while auto_button is on
	        	if (toomuchlight or motion or water) and not(isopen):			#if motion, light, or water is detected AND umbrella is not open
				#umbrella will open
				isopen=openFan()
				file.write("open \n"+str(datetime.datetime.now()+"\n\nlot light"+toomuchlight+"\n\nmove ment  "+motion+"\n\nwater  "+water+"\n\n")
			elif not(toomuchlight or motion or water) and isopen:		#if no motion, light, or water is detected AND umbrella is open
	                	#umbrella will close
				isopen = closeFan()												#otherwise do nothing
				file.write("auto close\n"+str(datetime.datetime.now())+"\n\n")

	            	if GPIO.input(buttonPin2) == 1:									#if second button is pressed again
	                	GPIO.output(ledPin2, GPIO.LOW)								#turn off led
	            		auto_button = False											#turn off auto
#				if isopen:													#if umbrella is open
#					GPIO.output(umbrellaPin, GPIO.LOW)						#close umbrella
#					isopen = False
	                	time.sleep(1)

		if GPIO.input(buttonPin):											#if manu_button is pressed
			manu_button = True												#button turns on
			GPIO.output(ledPin, GPIO.HIGH)									#first led turns on
			file.write("manu opened\n"+str(datetime.datetime.now())+"\n\n")
			time.sleep(1)
			openFan()
#			print(GPIO.input(buttonPin))									#print status of button

		while manu_button:													#while manual button is on
			GPIO.output(ledPin,GPIO.HIGH)									#led is on
#			if not(isopen):													#if umbrella is not open
#				GPIO.output(umbrellaPin, GPIO.HIGH)							#open umbrella
#				isopen = True
#				time.sleep(1)
			if GPIO.input(buttonPin):										#if button is pressed again
				GPIO.output(ledPin, GPIO.LOW)								#LED turns off
				manu_button = False
#				GPIO.output(umbrellaPin, GPIO.LOW)							#umbrella close
				isopen =closeFan()
				file.write("manu closed\n"+str(datetime.datetime.now())+"\n\n")
				time.sleep(1)
except KeyboardInterrupt:
	GPIO.Cleanup()

```


